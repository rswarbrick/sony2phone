#!/usr/bin/env python3

import argparse
import os
import re
import sys
from datetime import datetime
from typing import List, Optional, Set, Tuple

import pyexiv2  # type: ignore


def image_date(path: str) -> Optional[datetime]:
    '''Use exiv2 to get date stamp for the image at path'''
    im = pyexiv2.Image(path)
    exif = im.read_exif()
    dt_str = exif.get('Exif.Image.DateTime')
    if dt_str is None:
        return None
    assert isinstance(dt_str, str)
    try:
        # dt_str looks something like: 2021:01:04 18:30:42
        return datetime.strptime(dt_str, '%Y:%m:%d %H:%M:%S')
    except ValueError:
        return None


def date_to_phone_name(dt: datetime) -> str:
    return dt.strftime('%Y%m%d_%H%M%S')


def check_stem(stem: str,
               suff_idx: Optional[int],
               seen_paths: Set[str]) -> Optional[str]:
    ext_stem = f'{stem}({suff_idx})' if suff_idx is not None else stem
    path = ext_stem + '.jpg'
    if path in seen_paths:
        return None
    if os.path.exists(path):
        seen_paths.add(path)
        return None
    return path


def get_renames(paths: List[str]) -> Tuple[List[Tuple[str, str]], List[str]]:
    '''Compute the rename for each of the given images from its Exif data.

    Returns a list of renames, together with a list of images that haven't got
    a DateTime stamp.

    '''
    seen_paths = set()  # type: Set[str]
    work_list = []
    bad_paths = []
    for path in paths:
        dt = image_date(path)
        if dt is None:
            bad_paths.append(path)
        else:
            new_name = date_to_phone_name(dt)
            new_stem = os.path.join(os.path.dirname(path), new_name)
            new_path = check_stem(new_stem, None, seen_paths)
            if new_path is None:
                idx = 1
                while new_path is None:
                    new_path = check_stem(new_stem, idx, seen_paths)
                    idx += 1
            assert new_path is not None

            work_list.append((path, new_path))
            seen_paths.add(new_path)

    return (work_list, bad_paths)


def find_sources(path: str) -> List[str]:
    found = []
    for root, _, files in os.walk(path):
        for name in files:
            if re.match(r'DSC[0-9]+\.JPG$', name):
                found.append(os.path.join(root, name))
    return found


def main() -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument('root')
    parser.add_argument('--dry-run', '-n', action='store_true')

    args = parser.parse_args()

    sources = find_sources(args.root)
    renames, bad_files = get_renames(sources)
    if bad_files:
        print('Some files cannot be renamed:\n' +
              '\n'.join(bad_files),
              file=sys.stderr)

    for old, new in renames:
        if args.dry_run:
            print(f'{old} -> {new}')
        else:
            os.rename(old, new)

    return 1 if bad_files else 0


if __name__ == '__main__':
    main()
